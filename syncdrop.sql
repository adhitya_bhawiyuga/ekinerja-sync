-- DROP TABLE `ekinerja`.identitas_kepegawaian;

-- CREATE TABLE `ekinerja`.identitas_kepegawaian LIKE simpeg.identitas_kepegawaian;

-- INSERT INTO `ekinerja`.identitas_kepegawaian SELECT * FROM simpeg.identitas_kepegawaian; 

REPLACE INTO `ekinerja`.eselon SELECT * FROM simpeg.eselon;
REPLACE INTO `ekinerja`.golongan SELECT * FROM simpeg.golongan;
REPLACE INTO `ekinerja`.golruang SELECT * FROM simpeg.golruang;
REPLACE INTO `ekinerja`.identitas_kepegawaian SELECT * FROM simpeg.identitas_kepegawaian; 
REPLACE INTO `ekinerja`.identitas_pegawai SELECT * FROM simpeg.identitas_pegawai; 
REPLACE INTO `ekinerja`.jabatan SELECT * FROM simpeg.jabatan; 
REPLACE INTO `ekinerja`.jabatan_terakhir SELECT * FROM simpeg.jabatan_terakhir; 
REPLACE INTO `ekinerja`.keadaan_pegawai SELECT * FROM simpeg.keadaan_pegawai; 
REPLACE INTO `ekinerja`.pangkat_terakhir SELECT * FROM simpeg.pangkat_terakhir;
REPLACE INTO `ekinerja`.posisi SELECT * FROM simpeg.posisi;
REPLACE INTO `ekinerja`.status_pegawai SELECT * FROM simpeg.status_pegawai;
REPLACE INTO `ekinerja`.struktur_organisasi SELECT * FROM simpeg.struktur_organisasi;
REPLACE INTO `ekinerja`.sub_unitk SELECT * FROM simpeg.sub_unitk;
REPLACE INTO `ekinerja`.uniti SELECT * FROM simpeg.uniti;
REPLACE INTO `ekinerja`.unitk SELECT * FROM simpeg.unitk;
